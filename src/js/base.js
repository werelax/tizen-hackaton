define(["backbone"], function(Backbone) {
  "use strict";

  /* Generic Class */

  var Class = function() {
    this.initialize.apply(this, arguments);
  };

  _.extend(Class.prototype, {
    initialize: function() { }
  });

  Class.extend = Backbone.Model.extend;

  /* Vents */

  var Vent = function() {
    if (this.initialize) {
      this.initialize.apply({}, arguments);
    }
  };
  _.extend(Vent.prototype, Backbone.Events);

  /* Utility views */

  var Region = Class.extend({
    initialize: function(options) {
      options = options || {};
      this.selector = options.selector;
      this.$el = options.$el;
      this.currentView = false;
    },
    setElement: function($el) {
      this.$el = $el;
      if (this.currentView) { this.open(this.currentView); }
    },
    show: function(v) {
      if (this.currentView) { this.close(); }
      if (this.$el) { this.open(v); }
      this.currentView = v;
      v.parentView = this.parentView;
      v.trigger("show", this);
      if (v.onShow) { v.onShow(this); }
    },
    close: function() {
      this.currentView.stopListening();
      this.currentView.undelegateEvents();
      this.currentView.remove();
    },
    open: function(v) {
      if (this.$el) {
        v.render().$el.appendTo(this.$el.empty());
      }
    }
  });

  var Layout = Backbone.View.extend({
    constructor: function(options) {
      options = options || {};
      this.subviews = [];
      this.regions = options.regions || this.regions || {};
      this._regions = [];
      Backbone.View.apply(this, arguments);
      for (var k in this.regions) {
        if (this.regions.hasOwnProperty(k)) {
          if (typeof(this.regions[k]) === "string") {
            this[k] = new Region({selector: this.regions[k]});
          } else if (this.regions[k].$el) {
            this[k] = new this.regions[k].regionType({
              $el: this.regions[k].$el
            });
          } else if (this.regions[k].selector) {
            this[k] = new this.regions[k].regionType({
              selector: this.regions[k].selector
            });
          }
          this[k].parentView = this;
          this._regions.push(this[k]);
        }
      }
    },
    setElement: function() {
      Backbone.View.prototype.setElement.apply(this, arguments);
      this.attachRegions();
      this.attachSubViews();
    },
    attachRegions: function() {
      this._regions.forEach(function(region) {
        region.setElement(this.$(region.selector));
      }.bind(this));
    },
    addSubView: function(view, selector) {
      this.subviews.push({view: view.render(), selector: selector});
    },
    attachSubViews: function() {
      this.subviews.forEach(function(sv) {
        this.$(sv.selector).empty().append(sv.view.el);
      }.bind(this));
    },
    render: function() {
      this.$el.empty().append(this.template({}));
      this.attachRegions();
      this.attachSubViews();
      return this;
    },
  });

  var ItemView = Backbone.View.extend({
    render: function() {
      var data = this.serializeData();
      this.$el.empty().append(this.template(data));
      return this;
    },
    serializeData: function() {
      var source = this.model || this.data,
          data = source? source.toJSON() : {};
      if (typeof(this.helpers) === "function") {
        _.extend(data, this.helpers());
      } else {
        _.extend(data, this.helpers || {});
      }
      return data;
    },
  });

  var CollectionView = Backbone.View.extend({
    constructor: function() {
      Backbone.View.apply(this, arguments);
      this.views = {};
      this.listenTo(this.collection, "reset", this.render);
      this.listenTo(this.collection, "add", this.addItem);
      this.listenTo(this.collection, "remove", this.removeItem);
    },
    render: function() {
      if (this.itemContainer) {
        this.$el.html(this.template({}));
        this.$container = this.$(this.itemContainer);
      } else {
        this.$container = this.$el;
      }
      this.$container.empty();
      this.collection.each(this.addItem.bind(this));
      this.checkEmpty();
      return this;
    },
    addItem: function(m) {
      if (this.emptyCol) { this.$container.empty(); }
      this.emptyCol = false;
      var v = new this.itemView({model: m}).render();
      v.$el.appendTo(this.$container);
      v.parentView = this;
      v.trigger("show", this);
      if (v.onShow) { v.onShow(this); }
      this.views[m.cid] = v;
    },
    removeItem: function(m) {
      this.views[m.cid].remove();
      delete this.views[m.cid];
      this.checkEmpty();
    },
    checkEmpty: function() {
      if (this.collection.length === 0) {
        this.emptyCol = true;
        if (this.emptyView) {
          this.$container.empty().append((new this.emptyView()).render().$el);
        }
      }
    }
  });

  return {
    Class: Class,
    Vent: Vent,
    Region: Region,
    Layout: Layout,
    ItemView: ItemView,
    CollectionView: CollectionView
  };
});
