/* global navigator, window */
define([], function() {
  "use strict";

  var Camera = {
    urlBase: "/barqueue/bars/",
    takePhoto: function(videoTag) {
      // var canvas = document.querySelector("canvas");
      // var ctx = canvas.getContext("2d");
      // var snapshotButton = document.querySelector("");
      var localMediaStream = null;

      /*
      function snapshot() {
        if (localMediaStream) {
          ctx.drawImage(video, 0, 0);
          // "image/webp" works in Chrome 18. In other browsers, this will fall back to image/png.
          document.querySelector("img").src = canvas.toDataURL("image/webp");
        }
      }
      */

      // snappshotButton.addEventListener("click", snapshot, false);

      // Not showing vendor prefixes or code that works cross-browser.
      navigator.webkitGetUserMedia({video: true}, function(stream) {
        videoTag.src = window.URL.createObjectURL(stream);
        localMediaStream = stream;
      }, function(e){
        console.log(e);
      });
    }
  };

  return Camera;

});
