/* global navigator, google */
define([], function() {
  "use strict";

  var Location = {
    getCurrentLocation : function(handlePosition) {
      if (navigator && navigator.geolocation &&
          navigator.geolocation.getCurrentPosition) {
        navigator.geolocation.getCurrentPosition(function(position) {
          if (position.coords.latitude === 0 && position.coords.longitude === 0) {
            console.log("unable to acquire location");
          } else {
            handlePosition({latitude : position.coords.latitude, longitude : position.coords.longitude});
          }
        }, function(error) {
          console.log("GPS error occurred. Error code: ", JSON.stringify(error));
          handlePosition();
        });
      } else {
        console.log("No W3C Geolocation API available");
      }
    },
    distanceToLocation: function(barLocation, currentLocation) {
      var R = 6371*1000,
      dtr = Math.PI / 180,
      lat1 = barLocation.latitude*dtr, lon1 = barLocation.longitude*dtr,
      lat2 = currentLocation.latitude*dtr, lon2 = currentLocation.longitude*dtr,
      x = (lon2-lon1) * Math.cos((lat1+lat2)/2),
      y = (lat2-lat1),
      d = Math.sqrt(x*x + y*y) * R;
      d = Math.floor(d*100)/100;
      return d;
    },
    /* move this */
    drawLocationOnMaps: function(co, mapTag) {
      var mapOptions = {
        zoom: 18,
        center: new google.maps.LatLng(co.latitude, co.longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
      };
      var map = new google.maps.Map(mapTag.get(0), mapOptions);
      new google.maps.Marker({
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(co.latitude, co.longitude),
      });
    },
    reverseGeocode: function(co, cb) {
      var latlng = new google.maps.LatLng(co.latitude, co.longitude);
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({"latLng":latlng}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK && results[1]) {
          cb(results[1]);
        } else {
          cb(null);
        }
      });
    }
  };

  return Location;

});
