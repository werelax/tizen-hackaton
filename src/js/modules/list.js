define(["underscore", "backbone", "base", "components", "models", "templates", "modules/details", "lib/location"], function(_, Backbone, Base, Comp, models, JST, Details, Location) {
  "use strict";

  var vent = new Base.Vent(),
      currentPosition = {latitude: 0, longitude: 0};

  var ViewController = Comp.StackViewController.extend({
    initialize: function(options) {
      this.listenTo(vent, "selected:item", this.selected);
      this.listenTo(vent, "new:item", this.create);
      this.sub.navbar.addRightButton(new NewButton());
      this.delegate = options.delegate;
    },
    onShow: function() {
      this.push(new BarList({collection: this.collection}));
    },
    selected: function(item) {
      this.push(new Details.start(item));
    },
    create: function() {
      this.delegate.create();
    }
  });

  var NewButton = Base.ItemView.extend({
    template: JST["list/addbutton"],
    events: { "click": "addNew" },
    addNew: function() {
      vent.trigger("new:item");
    },
  });

  var ListItem = Base.ItemView.extend({
    tagName: "article",
    className: "post",
    template: JST["list/item"],
    events: { "click": "selected" },
    selected: function() {
      vent.trigger("selected:item", this.model);
    },
    helpers: {
      distance: function() {
        return Location.distanceToLocation(currentPosition, this.position);
      }
    }
  });

  var EmptyView = Base.ItemView.extend({
    tagName: "li",
    className: "topcoat-list__item",
    template: JST["list/empty"]
  });

  var BarList = Base.CollectionView.extend({
    initialize: function() {
      this.listenTo(this.collection, "change", this.render);
      this.listenTo(vent, "location:update", this.updatePosition);
    },
    itemView: ListItem,
    emptyView: EmptyView,
    className: "topcoat-list",
    itemContainer: ".content",
    template: JST["list/collection"],
    title: "BarQueue",
    updatePosition: function(pos) {
      currentPosition = pos;
      this.render();
    }
  });

  return {
    ViewController: ViewController,
    start: function(delegate, queue) {
      Location.getCurrentLocation(function(pos) {
        vent.trigger("location:update", pos);
      });
      return (new ViewController({
        collection: queue,
        delegate: delegate
      })).render();
    }
  };
});
