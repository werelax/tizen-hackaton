define(["backbone", "base", "templates", "lib/location"], function(Backbone, Base, JST, Location) {
  "use strict";

  var DetailsView = Base.ItemView.extend({
    template: JST["details/show"],
    title: function() {
      return this.model.get("title");
    },
    onShow: function() {
      Location.drawLocationOnMaps(this.model.get("position"), this.$("#map"));
    }
  });

  return {
    start: function(bar) {
      return (new DetailsView({model: bar})).render();
    }
  };
});
