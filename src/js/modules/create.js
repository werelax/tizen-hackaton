/* global navigator, window */
define(["underscore", "backbone", "base", "models", "templates", "lib/location"], function(_, Backbone, Base, Models, JST, Location) {
  "use strict";

  var vent = new Base.Vent();

  var ViewController = Base.Layout.extend({
    initialize: function(options) {
      this.delegate = options.delegate;
      this.sub = {
        navbar: new NavBar(),
        content: new CreateView({model: this.model})
      };
      this.listenTo(vent, "cancel", this.cancel);
      this.listenTo(vent, "save", this.save);
    },
    template: JST["create/layout"],
    regions: {
      navbar: ".nav-bar",
      content: ".content"
    },
    onShow: function() {
      this.navbar.show(this.sub.navbar);
      this.content.show(this.sub.content);
    },
    cancel: function() {
      this.delegate.list();
    },
    save: function(model) {
      this.delegate.save(model);
    }
  });

  var NavBar = Base.ItemView.extend({
    className: "topcoat-navigation-bar",
    template: JST["navbar"],
    events: { "click .js-back": vent.trigger.bind(vent, "cancel") }
  });

  var CreateView = Base.ItemView.extend({
    initialize: function() {
      _.bindAll(this, "onCapture", "captureError", "getLocation");
      Location.getCurrentLocation(this.getLocation);
    },
    template: JST["create/main"],
    events: {
      "click .js-save-button": "save",
      "click .js-snapshot-button": "snapShot",
      "keyup .js-name": "saveName"
    },
    save: function() {
      vent.trigger("save", this.model);
    },
    saveName: function() {
      this.model.set({ title: this.$(".js-name").val() });
    },
    render: function() {
      this.constructor.__super__.render.call(this);
      this.startCapture();
      return this;
    },
    startCapture: function() {
      this.videoTag = this.$("video").get(0);
      navigator.webkitGetUserMedia(
        {video: true},
        this.onCapture,
        this.captureError
      );
    },
    onCapture: function(stream) {
      this.videoTag.src = window.URL.createObjectURL(stream);
      this.mediaStream = stream;
    },
    captureError: function() {
      window.alert("No puedo iniciar la cámara!");
    },
    snapShot: function() {
      var $canvas = this.$("canvas"),
          canvas = $canvas.get(0),
          ctx = canvas.getContext("2d"),
          video = this.videoTag,
          $img = this.$(".js-snapshot"),
          img = $img.get(0),
          ratio = 320/240;
      ctx.drawImage(this.videoTag, 0, 0, video.videoWidth, video.videoWidth/ratio, 0, 0, 320, 240);
      img.src = canvas.toDataURL("image/png");
      $(this.videoTag).hide();
      this.$(".js-button-holder").empty().append("<i class='fa fa-check fa-2x'/>");
      $img.show();
      // save the image
      this.model.set({ img: canvas.toDataURL() });
    },
    getLocation: function(pos) {
      this.model.set({ position: pos });
      this.$(".js-location").val(pos.latitude + ", " + pos.longitude);
      Location.reverseGeocode(pos, this.setAddress.bind(this));
    },
    setAddress: function(addrs) {
      if (addrs) {
        console.log(addrs);
        this.model.set({address: addrs["formatted_address"]});
        this.$(".js-location").val(addrs["formatted_address"]);
      }
    }
  });

  return {
    start: function(delegate) {
      var bar = new Models.Bar(),
          createModule = new ViewController({ delegate: delegate, model: bar });
      return createModule.render();
    }
  };
});
