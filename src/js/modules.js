define(["modules/list", "modules/details", "modules/create"],
       function(List, Details, Create) {
  "use strict";
  return {
    List: List,
    Details: Details,
    Create: Create
  };
});
