define(['handlebars'], function(Handlebars) {

this["JST"] = this["JST"] || {};

this["JST"]["create/layout"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<section class=\"total-height content create-page\" style=\"overflow:scroll\">\n</section>\n<div class=\"nav-bar\">\n</div>\n";
  });

this["JST"]["create/main"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<article>\n\n  <div class=\"ok-button js-save-button\">\n    <img src=\"assets/img/OK.png\">\n  </div>\n\n  <div class=\"row\">\n    <div class=\"t fw\">\n      <div class=\"tr\">\n        <div class=\"td\">\n          <i class=\"fa fa-map-marker fa-3x\"></i>\n        </div>\n        <div class=\"td\">\n          <input type=\"text\" class=\"content-cell js-location\"></input>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <div class=\"row\">\n    <div class=\"fw capture\">\n      <video autoplay class=\"fw\" width=\"320\" height=\"240\"></video>\n      <img src=\"\" class=\"fw js-snapshot\">\n      <canvas style=\"display:none;\" class=\"fw\" width=\"320\" height=\"240\"></canvas>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"t fw\">\n      <div class=\"tr\">\n        <div class=\"td\">\n          <input type=\"text\" class=\"content-cell js-name\" placeholder=\"Nombre\"></input>\n        </div>\n        <div class=\"td icon-cell js-button-holder\">\n          <img src=\"assets/img/camara-normal.png\" class=\"fw js-snapshot-button\"/>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</article>\n";
  });

this["JST"]["create/navbar"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"topcoat-navigation-bar__item js-back\" style=\"float:left\">\n  <button class=\"topcoat-button js-back-button\">Back</button>\n</div>\n<div class=\"topcoat-navigation-bar__item center full\">\n  <h1 class=\"topcoat-navigation-bar__title js-title\">\n    Nombre del Bar\n  </h1>\n</div>\n";
  });

this["JST"]["details/layout"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<section class=\"component nav-bar\" style=\"position:fixed; width:100%; z-index:9999\">\n</section>\n<section class=\"component content\" style=\"padding-top:69px\">\n</section>\n";
  });

this["JST"]["details/show"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div id=\"map\"></div>\n";
  });

this["JST"]["layout"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"container\">\n</div>\n";
  });

this["JST"]["list/addbutton"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<a href=\"#\" class=\"addNew\">+</a>\n";
  });

this["JST"]["list/collection"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<header>\n  <!--\n  <nav>\n  <ul>\n    <li><a href=\"#\">Near</a></li>\n    <li><a href=\"#\">Cheap</a></li>\n    <li class=\"active\"><a href=\"#\">Nice</a></li>\n  </ul>\n  </nav>\n  -->\n  </header>\n\n  <!-- POSITION ABSOLUTE -->\n  <!-- header (absoluto) -->\n  <!--  section (absoluto+aqui aparece el scroll (alto fijo)) -->\n  <!-- el que desborda! -->\n\n<section class=\"content\">\n</section>\n";
  });

this["JST"]["list/empty"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "";


  return buffer;
  });

this["JST"]["list/header"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<hgroup>\n  <div class=\"cartel\">\n    <h1>Bar Queue</h1>\n  </div>\n  <a href=\"#\">+</a>\n</hgroup>\n";
  });

this["JST"]["list/item"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<img src=\"";
  if (stack1 = helpers.img) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.img; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" width=\"107px\"/ height=\"80px\">\n<div class=\"meta\">\n  <h1>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1>\n  <p class=\"address\">";
  if (stack1 = helpers.address) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.address; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n  <p class=\"distance\">";
  if (stack1 = helpers.distance) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.distance; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " m</p>\n</div>\n";
  return buffer;
  });

this["JST"]["list/layout"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<section class=\"total-height content\" style=\"overflow:scroll\">\n</section>\n<div class=\"nav-bar\">\n</div>\n";
  });

this["JST"]["navbar"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<header class=\"nav-bar toldo\">\n  <hgroup>\n    <div class=\"cartel\">\n      <h1 class=\"js-title\">Bar Queue</h1>\n    </div>\n    <span class=\"js-right-button\"></span>\n  </hgroup>\n  <div class=\"js-back\">\n    <div class=\"js-back-button\">\n      <img src=\"assets/img/retroceso-normal.png\">\n    </div>\n  </div>\n</header>\n";
  });

this["JST"]["sample"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<h1> Just a sample </h1>\n";
  });

return this["JST"];

});