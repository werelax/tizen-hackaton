/* global window */
define(["underscore", "backbone", "base", "templates"], function(_, Backbone, Base, JST) {
  "use strict";

  var AnimatedRegion = Base.Region.extend({
    show: function(view, dir) {
      this.forward = !dir; // true or false
      Base.Region.prototype.show.call(this, view);
    },
    animationClass: "slide",
    open: function(view) {
      this.el = this.$el.get(0);
      var from = this.currentView,
          anim = this.animationClass,
          inName = (this.forward? "in" : "out"),
          outName = (this.forward? "out" : "in");
      if (!from) {
        Base.Region.prototype.open.call(this, view);
      } else {
        this.$el.append(view.render().el);
        view.$el.removeClass("center " + outName).addClass(anim + " trans " + inName);
        view.el.scrollTop = 0;
        this.el.offsetWidth = this.el.offsetWidth;
        from.$el.one("webkitTransitionEnd", _.bind(function() {
          from.stopListening();
          from.remove();
        }, this));
        view.$el.one("webkitTransitionEnd", function() {
          view.$el.removeClass("center").addClass("final");
        });
        view.$el.removeClass(inName).addClass("center");
        from.$el.removeClass("center " + inName).addClass(anim + " trans " + outName);
      }
    },
    close: function() {
      return "Here to override parent";
    }
  });

  var SlideRegion = AnimatedRegion.extend({
    animationClass: "slide"
  });

  var StackViewController = Base.Layout.extend({
    constructor: function() {
      this.currentView = false;
      this.views = [];
      this.sub = {navbar: new NavBar({
        viewController: this
      })};
      Base.Layout.apply(this, arguments);
      this.listenTo(this, "show", this.attachSubviews);
    },
    template: JST["list/layout"],
    regions: {
      navbar: ".nav-bar",
      content: {
        regionType: SlideRegion,
        selector: ".content"
      },
    },
    attachSubviews: function() {
      this.navbar.show(this.sub.navbar);
    },
    push: function(view) {
      if (this.currentView) {
        this.views.push(this.currentView);
      }
      window.scrollTo(0,0);
      this.currentView = view;
      this.sub.navbar.push(view);
      this.content.show(view);
    },
    pop: function() {
      if (this.views.length > 0) {
        this.currentView = this.views.pop();
        this.content.show(this.currentView, true);
        this.sub.navbar.pop();
      }
    },
  });

  var NavBar = Base.ItemView.extend({
    initialize: function(options) {
      this.viewController = options.viewController;
      this.views = [];
      this.currentView = false;
      this.rightButtonRegion = new Base.Region();
    },
    className: "topcoat-navigation-bar",
    template: JST["navbar"],
    events: { "click .js-back-button": "popView" },
    onShow: function() {
      this.rightButtonRegion.setElement(this.$(".js-right-button"));
    },
    push: function(view) {
      if (this.currentView) {
        this.views.push(this.currentView);
      }
      this.currentView = view;
      this.activateCurrentView();
    },
    pop: function() {
      if (this.views.length > 0) {
        this.currentView = this.views.pop();
        this.activateCurrentView();
      }
    },
    popView: function() {
      this.viewController.pop();
    },
    previousView: function() {
      return this.views[this.views.length - 1];
    },
    activateCurrentView: function() {
      var view = this.currentView;
      // var prevView = this.previousView();
      this.currentView = view;
      this.$(".js-title").text(_.result(view, "title"));
      if (this.views.length > 0) {
        this.$(".js-back").show();
        /*
        this.$(".js-back-button").text(
          (prevView && _.result(prevView, "title")) || "Back"
        );
        */
      } else {
        this.$(".js-back").hide();
      }
    },
    addRightButton: function(view) {
      this.rightButtonRegion.show(view);
    },
    removeRightButton: function() {
      this.rightButtonRegion.$el.empty();
    }
  });

  return {
    StackViewController: StackViewController,
    AnimatedRegion: AnimatedRegion
  };

});
