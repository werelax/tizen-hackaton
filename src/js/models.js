define(["backbone", "localsync", "lib/location"], function(Backbone, localsync, Location) {
  "use strict";

  var currentLocation;

  var Bar = Backbone.Model.extend({
    defaults: {
      title: "Un bar",
      img: "assets/img/restaurante1.jpg",
      position: {latitude: 0, longitude: 0}
    }
  });

  var BarCollection = Backbone.Collection.extend({
    initialize: function() {
      if (currentLocation) {
        this.currentLocation = currentLocation;
      } else {
        Location.getCurrentLocation(this.setCurrentLocation.bind(this));
      }
    },
    url: "/queue",
    model: Bar,
    setCurrentLocation: function(pos) {
      this.currentLocation = pos;
      currentLocation = pos;
      this.sort();
      this.trigger("change");
    },
    comparator: function(m) {
      if (this.currentLocation) {
        return Location.distanceToLocation(this.currentLocation, m.get("position"));
      } else {
        return m.get("title");
      }
    }
  });

  return {
    Bar: Bar,
    BarCollection: BarCollection
  };

});
