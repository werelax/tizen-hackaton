/*global window, document, navigator */
define(
  ["backbone", "jquery", "hammer", "templates", "base", "components", "modules", "models"],
    function(Backbone, $, hammer, JST, Base, Comp, modules, Models) {
    "use strict";

    var FlipRegion = Comp.AnimatedRegion.extend({
      animationClass: "flip"
    });

    var AppLayout = Base.Layout.extend({
      className: "trans flip parent",
      template: JST["layout"],
      regions: {
        main: {
          selector: ".container",
          regionType: FlipRegion
        }
      }
    });

    var AppDelegate = Base.Class.extend({
      initialize: function() {
        this.app = new AppLayout().render();
        this.app.$el.appendTo("body");
        this.collection = new Models.BarCollection();
        this.collection.fetch();
        this.app.main.show(modules.List.start(this, this.collection));
      },
      create: function() {
        this.app.main.show(modules.Create.start(this));
      },
      save: function(bar) {
        this.collection.add(bar);
        bar.save();
        this.app.main.show(modules.List.start(this, this.collection), true);
      },
      list: function() {
        this.app.main.show(modules.List.start(this, this.collection), true);
      }
    });

    /* Initialize */

    function start() {
      var app = new AppDelegate();

      return app;
    }

    $(function() {
      if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android)/) && !window.deviceReady) {
        document.addEventListener("deviceready", start, false);
      } else {
        start();
      }
    });
  }
);
